<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <link href="assets/stylesheets/main.css" rel="stylesheet" type="text/css">
  <title><?= $locals["pageTitle"]; ?></title>

</head>

<body>
	<div class="content">
		<img style="width: 100%" src="assets/images/banner.jpg" alt="EAT FOOD Banner">
	</div>
	<div class="content">
	  <nav>
		<li><a href= '<?= BASE_PATH ?>/'>Home</a></li>
		<li><a href= '<?= BASE_PATH ?>/login'>Log In</a></li>
		<li><a href= '<?= BASE_PATH ?>/profile'>Profile Page</a></li>
		<li><a href= '<?= BASE_PATH ?>/logout'>Log Out</a></li>
		<li><a href= '<?= BASE_PATH ?>/register'>Register</a></li>
		<li><a href= '<?= BASE_PATH ?>/order'>Order</a></li>
		<li><a href= '<?= BASE_PATH ?>/orders'>Orders List</a></li>
	  </nav>
	
		<?= \Rapid\Renderer::VIEW_PLACEHOLDER ?>
		
	 </div>
</body>
</html>