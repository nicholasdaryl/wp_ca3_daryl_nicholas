<h1>Here is the list of all of <?= $locals["username"] ?>'s orders</h1>
<?php if (empty($locals["orders"])) { ?>
	<h2>You haven't made any orders as of now.</h2>
<?php } ?>
<?php if ($_GET["update_success"]) { ?>
	<h2>You have successfully updated an order.</h2>
<?php } ?>
<?php if ($_GET["delete_success"]) { ?>
	<h2>You have successfully deleted an order.</h2>
<?php } ?>

<?php foreach ($locals["orders"] as $order) { ?>
	<div class="orderDiv">
		<?php 
			if ($_GET["update"] && $_GET["order_id"] == $order["order_id"]) { ?>
				<form action="<?= BASE_PATH ?>/orders?update=1&order_id=<?php echo $order["order_id"]; ?>" method="post">
					<input type="text" name="new_item" placeholder="New food name">
					<input type="submit" value="Change">
					<?php echo $locals["messages"]["name_error"]?>
				</form>
	   <?php } else {
				echo $order["item_name"]; ?>
				<a class="btn" href="<?= BASE_PATH ?>/orders?update=1&order_id=<?php echo $order["order_id"]; ?>">Change</a> 
				<a class="btn" href="<?= BASE_PATH ?>/deleteOrder?order_id=<?php echo $order["order_id"]; ?>">Delete</a> <?php
			}
		?>
		
	</div>
	<br> <!--remove this later-->
<?php } ?>