<h1><?= $locals["user"]["email"] ?>'s Profile</h1>

<?php if ($_GET["success"]) { ?>
	<h2>You have successfully updated your account.</h2>
<?php } ?>

Username:<br>
<?php if ($_GET["edit_username"]) { ?>
	<form action="<?= BASE_PATH ?>/profile?update=1&edit_username=1" method="post">
		<input type="text" name="new_username" placeholder="New email address">
		<input type="submit" value="Edit">
		<?php echo $locals["messages"]["username_error"]?>
	</form>
<?php } else { ?>
	<?= $locals["user"]["email"] ?><br>
	<a class="btn" href="<?= BASE_PATH ?>/profile?update=1&edit_username=1">Edit</a>
<?php } ?>
<br><br>
<?php if ($_GET["edit_address"]) { ?>
	<form action="<?= BASE_PATH ?>/profile?update=1&edit_address=1" method="post">
		Address line 1(Street name)<br>
		<input type="text" name="new_address_1"> <?php echo $locals["messages"]["address_1_error"]?><br>
		Address line 2(City)<br>
		<input type="text" name="new_address_2"> <?php echo $locals["messages"]["address_2_error"]?><br>
		County<br>
		<input type="text" name="new_address_3"> <?php echo $locals["messages"]["address_3_error"]?><br><br>
		
		<input type="submit" value="Edit">
	</form>
<?php } else { ?>
Address:<br>
<?= $locals["user"]["address"]?><br>
<a class="btn" href="<?= BASE_PATH ?>/profile?update=1&edit_address=1">Edit</a>
<?php } ?>
<br><br>
<a class="btn" href="<?= BASE_PATH ?>/deleteAccount">Delete this account</a>