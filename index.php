<?php
	try {
		
		// Include the Rapid library
		require_once('lib/Rapid.php');

		// Create a new Router instance
		$app = new \Rapid\Router();
		
		define("BASE_PATH", "/wp_ca3_daryl_nicholas");
		
		// Define some routes. Here: requests to / will be
		// processed by the controller at controllers/Home.php
		$app->GET('/', 'home');
		$app->GET('/orders', 'orders');
		$app->GET('/order', 'order');
		$app->GET('/login', 'login');
		$app->GET('/logout', 'logout');
		$app->GET('/register', 'register');
		$app->GET('/profile', 'profile');
		$app->POST('/login', 'login');
		$app->POST('/register', 'register');
		$app->POST('/order', 'order');
		$app->POST('/orders', 'orders');
		$app->GET('/deleteOrder', 'deleteOrder');
		$app->GET('/deleteAccount', 'deleteAccount');
		$app->POST('/profile', 'profile');

		// Process the request
		$app->dispatch();
		
	} catch (\Rapid\RouteNotFoundException $e) {
		http_response_code(404);
		echo "404: PAGE NOT FOUND";
	} catch (Exception $e) {
		http_response_code(500);
		exit();
	}

?>