#README for CA3 Web Programming
I decided that I am going to be doing a food delivery type of website. Complete with a log in system and the ability to order food.
I will have two tables, one called users and another called orders. Users can have many orders but an order can only belong to one user.

##Project set-up
First thing I did was to copy the config.php.sample and renamed it to config.php with all the details specific to my database

Next I made my two tables of customers and orders.
Here is a screenshot of me having made the tables:

![](/assets/images/screenshots/1.PNG)

Here are the descriptions of both tables:

Orders:
![](/assets/images/screenshots/2.PNG)
Users:
![](/assets/images/screenshots/3.PNG)

Next thing I did was to research about how to do a log in system. I was wondering about how I can keep the user logged in after leaving the
login page with a successful log in. I found out about PHP Sessions. 

Resources:
[W3Schools](https://www.w3schools.com/php/php_sessions.asp) 

So, next I made a simple implementation of what I just learnt, by testing out the $_SESSION global array just to see if it works
I wrote this piece of test code:

![](/assets/images/screenshots/5.PNG)
![](/assets/images/screenshots/4.PNG)

I made a test user to test it out and it worked just as I expected it to.

After that I went on and made the login page by checking if the user inputs match the user stored on the database.

I made it display an error message if no match is found:
![](/assets/images/screenshots/9.PNG)
![](/assets/images/screenshots/10.PNG)
![](/assets/images/screenshots/11.PNG)
![](/assets/images/screenshots/12.PNG)

and store the user_id in the $_SESSION array if a match is found and just redirects to the home page
![](/assets/images/screenshots/13.PNG)

After I got that working, i moved on to doing the register page, which creates a new account.

first I made the forms and did a simple validation to check whether the fields are empty. If so, then I add to a $messages array and error message
specific to the field that will later on be passed through the $locals array provided from the render method.

![](/assets/images/screenshots/6.PNG)
![](/assets/images/screenshots/14.PNG)

Next, I moved on doing the add order page. It's just a page with a form with one field taking in a string. I took my first shot at properly validating input here,
simply because I only had to deal with one field.

I checked for two things. One is that the field cannot be empty and another is that the input cannot have any special characters other than words or numbers.
I created myself a regex, using the website [Regex 101](https://regex101.com/) as my regex tester and used the preg_match() function to validate.

![](/assets/images/screenshots/8.PNG)
![](/assets/images/screenshots/15.PNG)
![](/assets/images/screenshots/16.PNG)
![](/assets/images/screenshots/17.PNG)

Next, I did my display orders page. Just did a select statement to select the orders from the user which is logged in and I made two anchors as the edit and delete button

![](/assets/images/screenshots/19.PNG)

I had an idea of making the edit and delete take place in the same page. So, if users wanted to change an order, the page will refresh with an input field instead of displaying the value of which the user wants to change.

![](/assets/images/screenshots/20.PNG)
![](/assets/images/screenshots/21.PNG)
![](/assets/images/screenshots/22.PNG)

Next i did the delete query which was simple enough because I am deleting from a child table so I wouldn't need to worry about referential integrity.

![](/assets/images/screenshots/23.PNG)

Once i had the CRUD operations working for the orders table, I went on and did similar things to the user profile page.

![](/assets/images/screenshots/24.PNG)
![](/assets/images/screenshots/25.PNG)
![](/assets/images/screenshots/26.PNG)

For the delete account, I made a trigger in the database to delete all records from the orders table before deleting the related user.

![](/assets/images/screenshots/30.PNG)
![](/assets/images/screenshots/31.PNG)

After a successful deletion the user will then be taken back to the home page.

![](/assets/images/screenshots/9.PNG)

Then I also put checks in the view order and make order pages, to see if the user is logged in. And if not then it will just direct them to the login page passing on different messages.

![](/assets/images/screenshots/28.PNG)
![](/assets/images/screenshots/29.PNG)

I also handled if a user does not have any orders.

![](/assets/images/screenshots/27.PNG)

Then, I went back to do more validations on user registration and developed an email regex again using [Regex 101](https://regex101.com/) to help. 
For the password regex, I had a hard time figuring out how to make a regex for password validation, as there is no specific order to how the users are allowed to create their passwords.
I ended up going online searching for ideas on how to do it, and found a thread from [Stack Overflow](https://stackoverflow.com/questions/8141125/regex-for-password-php), which gave me the idea to do it by multiple different checks instead of one big long regex.

So here is a look at my email and password regexes:

![](/assets/images/screenshots/32.PNG)

Lastly I wrapped everything on index.php with the try catch blocks. I didn't make an error page, probably should have, but i just have it echo out a message just to see it working.

![](/assets/images/screenshots/33.PNG)

I did a few css just to center the content and added in a silly looking banner image which i made on paint program:

![](/assets/images/screenshots/34.PNG)

I made changes to all my queries to be prepared statements because prepared statements ensure the consistency of the statements

Here is a snippet of one of them:
![](/assets/images/screenshots/35.PNG)

I made two pages to separate the delete queries, which was previously joined under the same controller for viewing and editing.

##Things I can Improve

- It is better to encrypt the password being stored
- It would make more sense to store more information on the order like the quantity, also having it being ordered from a fixed set of available menus.
- Having like a shopping basket for each order would be a nice thing to do
- Styling, Obviously. 