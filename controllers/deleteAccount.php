<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	session_start();
	
	$user_id = $_SESSION["user_id"] ?? NULL;

	$statement = $db->prepare('DELETE FROM users WHERE user_id = ?');
	$statement->bindParam(1, $user_id);
	$statement->execute();
	$response->redirect("/logout");

} ?>