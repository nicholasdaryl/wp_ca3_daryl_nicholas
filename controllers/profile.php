<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	session_start();
	
	//$_SESSION["user_id"] = 1; //----------------- commented out, just used for testing purposes
	$messages = [];
	$user = [];
	$regex = "@^.*[^a-zA-Z0-9 \t].*$@";
	$email_regex = "%^[^\W](\w*((\.\w+)?))@([a-zA-Z0-9]+(\.\w+))+%";
	$user_id = $_SESSION["user_id"] ?? NULL;
	
	//check if the user is already logged in otherwise redirect to the login page passing a status
	if ($user_id == NULL) {
		$response->redirect("/login");
	} else {
		// user object
		$statement = $db->query('SELECT * FROM users WHERE user_id = '.$user_id);
		$user = $statement->fetch();
	}
	
	//checks if anything is put up on the query string for update
	if ($request->method() == "POST") {
		//check if users want to update
		if ($request->query("update")) {
			// check if user wants to edit username
			if ($request->query("edit_username")) {
				//validations here
				if (!preg_match($email_regex, $request->body("new_username"))) {
					$messages["username_error"] = "That is not a valid e-mail address.";
				}
				if (empty($request->body("new_username"))) {
					$messages["username_error"] = "Username cannot be empty.";
				}
				
				if (empty($messages)) {
					$statement = $db->prepare('UPDATE users SET email = ? WHERE user_id = ?');
					$statement->bindParam(1, $request->body("new_username"));
					$statement->bindParam(2, $user["user_id"]);
					$statement->execute();
					$response->redirect("/profile?success=1");
				}
			}
			// check if user wants to edit username
			if ($request->query("edit_address")) {
				//validations here
				if (empty($request->body("new_address_1"))) {
					$messages["address_1_error"] = "Street name cannot be empty.";
				}
				if (preg_match($regex, $request->body("new_address_1"), $matches)) {
					$messages["address_1_error"] = "You cannot have illegal characters in the order.";
				}
				if (empty($request->body("new_address_2"))) {
					$messages["address_2_error"] = "City name cannot be empty.";
				}
				if (preg_match($regex, $request->body("new_address_2"), $matches)) {
					$messages["address_2_error"] = "You cannot have illegal characters in the order.";
				}
				if (empty($request->body("new_address_3"))) {
					$messages["address_3_error"] = "County name cannot be empty.";
				}
				if (preg_match($regex, $request->body("new_address_3"), $matches)) {
					$messages["address_3_error"] = "You cannot have illegal characters in the order.";
				}
				// if no errors then execute update
				if (empty($messages)) {
					$fullAddress = $request->body("new_address_1")."<br>".$request->body("new_address_2")."<br>".$request->body("new_address_3");
					$statement = $db->prepare('UPDATE users SET address = ? WHERE user_id = ?');
					$statement->bindParam(1, $fullAddress);
					$statement->bindParam(2, $user["user_id"]);
					$statement->execute();
					$response->redirect("/profile?success=1");
				}
			}
		}
	}
	
	//echo $username; //-------------------------------- for testing purposes
	
	$response->render('main', 'profile', [
	"pageTitle" => "Profile Page",
	"messages" => $messages,
	"user" => $user
	]);
} ?>