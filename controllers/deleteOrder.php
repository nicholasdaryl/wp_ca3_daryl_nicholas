<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	$statement = $db->prepare('DELETE FROM orders WHERE order_id = ?');
	$statement->bindValue(1, $request->query("order_id"));
	$statement->execute();
	$response->redirect("/orders?delete_success=1");
	
} ?>