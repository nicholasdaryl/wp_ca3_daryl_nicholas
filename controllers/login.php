<?php return function($request, $response) {
	$db = require_once('lib/database.php');
	
	session_start();
	
	//$_SESSION["user_id"] = 1; ----------------- commented out, just used for testing purposes
	$username = NULL;
	$view_name = NULL;
	$message = NULL;
	$user_id = $_SESSION["user_id"] ?? NULL;
	
	//check if the user is still logged in. if not then just display "Guest" as username
	if ($user_id != NULL) {
		$view_name = "alreadyLoggedIn";
		//retrieve email
		$statement = $db->query('SELECT * FROM users WHERE user_id = '.$user_id);
		$resultSet = $statement->fetch();
		$username = $resultSet['email'];
	} else {
		// retrieve username(email) from database and put it into $username to be passed into the render method
		$view_name = "login";
		if ($request->method() == "POST") {
			$statement = $db->query('SELECT * FROM users WHERE email = "'.$request->body("username").'" AND password = "'.$request->body("password").'"');
			$resultSet = $statement->fetch();
			if (empty($resultSet)) {
				$message = "Invalid username or password.";
			} else {
				$_SESSION["user_id"] = $resultSet["user_id"];
				$response->redirect("/");
			}
		}
	}
	
	$response->render('main', $view_name, [
	"pageTitle" => "Log In",
	"username" => $username,
	"message" => $message
	]);
} ?>