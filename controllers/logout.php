<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	$config = require("config.php");
	
	session_start();
	
	$user_id = $_SESSION["user_id"] ?? NULL;
	
	//if the user is logged in then log him/her out
	if ($user_id != NULL) {
		$_SESSION["user_id"] = NULL;
		$response->redirect("/");
	} else {
		$response->redirect("/login");
	}
} ?>