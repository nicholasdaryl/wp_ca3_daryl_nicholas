<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	session_start();
	
	//$_SESSION["user_id"] = 1; //----------------- commented out, just used for testing purposes
	$messages = [];
	$resultSet= [];
	$username = NULL;
	$regex = "@^.*[^a-zA-Z0-9 \t].*$@";
	$user_id = $_SESSION["user_id"] ?? NULL;
	
	//check if the user is already logged in otherwise redirect to the login page passing a status
	if ($user_id == NULL) {
		$response->redirect("/login?ordersList=1");
	} else {
		// retrieve username(email) from database and put it into $username to be passed into the render method
		$statement = $db->prepare('SELECT * FROM users WHERE user_id = ?');
		$statement->bindParam(1, $user_id);
		$statement->execute();
		$resultSet = $statement->fetch();
		$username = $resultSet['email'];
		// retrieve orders
		$statement = $db->prepare('SELECT * FROM orders WHERE user_id = ?');
		$statement->bindParam(1, $user_id);
		$statement->execute();
		$resultSet = $statement->fetchAll();
	}
	//checks if anything is put up on the query string for update
	if ($request->method() == "POST") {
		//validations here
		if (empty($request->body("new_item"))) {
			$messages["name_error"] = "Food name cannot be empty.";
		}
		if (preg_match($regex, $request->body("new_item"), $matches)) {
			$messages["name_error"] = "Food name cannot have illegal characters.";
		}
		
		if (empty($messages)) {
			$statement = $db->prepare('UPDATE orders SET item_name = "'.$request->body("new_item").'" WHERE order_id = ?');
			$statement->bindParam(1, $request->query("order_id"));
			$statement->execute();
			$response->redirect("/orders?update_success=1");
		}
	}
	
	//echo $username; //-------------------------------- for testing purposes
	
	$response->render('main', 'orders', [
	"pageTitle" => "Orders List",
	"messages" => $messages,
	"username" => $username,
	"orders" => $resultSet
	]);
} ?>