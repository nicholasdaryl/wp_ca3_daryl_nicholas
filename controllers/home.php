<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	session_start();
	
	//$_SESSION["user_id"] = 1; //----------------- commented out, just used for testing purposes
	$username = NULL;
	$user_id = $_SESSION["user_id"] ?? NULL;
	
	//check if the user is still logged in. if not then just display "Guest" as username
	if ($user_id == NULL) {
		$username = "Guest";
	} else {
		// retrieve username(email) from database and put it into $username to be passed into the render method
		$statement = $db->query('SELECT * FROM users WHERE user_id = '.$user_id);
		$resultSet = $statement->fetch();
		$username = $resultSet['email'];
	}
	
	//echo $username; //-------------------------------- for testing purposes
	
	$response->render('main', 'home', [
	"pageTitle" => "EAT FOOD - Home",
	"username" => $username]);
} ?>