<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	$messages = [];// associative array corresponding to the invalid attributes
	
	/*
		checks the basic foo@bar.bla format
		with the addition of not allowing it to have leading and consecutive dots i.e .foo..@bar.bla
		and has to include at least 1 dot at after the @ symbol i.e foo@bar (is invalid), foo@.bar (also invalid)
	*/
	$email_regex = "%^[^\W](\w*((\.\w+)?))@([a-zA-Z0-9]+(\.\w+))+%";
	
	$password_error_message = "Password not strong enough (At least 8 characters, 1 uppercase, 1 lowercase and 1 digit)";
	//checks if password length is at least 8
	$pass_regex_1 = "@.{8,}@";
	//checks if it has digits
	$pass_regex_2 = "@[0-9]@";
	//checks if it has capital letters
	$pass_regex_3 = "@[A-Z]@";
	//checks if it has lowercase letters
	$pass_regex_4 = "@[a-z]@";
	
	$specialChar_regex = "@^.*[^a-zA-Z0-9 \t].*$@";
	
	//check if there is something posted, if there is then add to the users table
	if ($request->method() == "POST") {
		
		//validations here
		
		//email checks
		if (!preg_match($email_regex, $request->body("username"))) {
			$messages["username_error"] = "That is not a valid e-mail address.";
		} else {
			//retrieve all the email addresses
			$statement = $db->query('SELECT email FROM users');
			$resultSet = $statement->fetchAll();
			$statement->closeCursor();
			// check if email is already taken
			foreach ($resultSet as $email) {
				if ($request->body("username") == $email["email"]) {
					$messages["username_error"] = "e-mail address already taken.";
					break;
				}
			}
		}
		
		if (empty($request->body("username"))) {
			$messages["username_error"] = "Username cannot be empty.";
		}
		// Password checks
		if (!preg_match($pass_regex_1, $request->body("password"), $matches)) {
			$messages["password_error"] = $password_error_message;
		}
		if (!preg_match($pass_regex_2, $request->body("password"), $matches)) {
			$messages["password_error"] = $password_error_message;
		}
		if (!preg_match($pass_regex_3, $request->body("password"), $matches)) {
			$messages["password_error"] = $password_error_message;
		}
		if (!preg_match($pass_regex_4, $request->body("password"), $matches)) {
			$messages["password_error"] = $password_error_message;
		}
		if (empty($request->body("password"))) {
			$messages["password_error"] = "Password cannot be empty.";
		}
		//address checks
		if (empty($request->body("address_1"))) {
			$messages["address_1_error"] = "Address cannot be empty.";
		}
		if (preg_match($specialChar_regex, $request->body("address_1"), $matches)) {
			$messages["address_1_error"] = "You cannot have illegal characters in the order.";
		}
		if (empty($request->body("address_2"))) {
			$messages["address_2_error"] = "City cannot be empty.";
		}
		if (preg_match($specialChar_regex, $request->body("address_2"), $matches)) {
			$messages["address_2_error"] = "You cannot have illegal characters in the order.";
		}
		if (empty($request->body("address_3"))) {
			$messages["address_3_error"] = "County cannot be empty.";
		}
		if (preg_match($specialChar_regex, $request->body("address_3"), $matches)) {
			$messages["address_3_error"] = "You cannot have illegal characters in the order.";
		}
		
		
		// if there are no error messages then add the user and automatically log in
		if (empty($messages)) {
			$fullAddress = $request->body("address_1")."<br>".$request->body("address_2")."<br>".$request->body("address_3");
			$data = [
				"email" => $request->body("username"),
				"password" => $request->body("password"),
				"address" => $fullAddress
			];
			// use of prepared statement
			$statement = $db->prepare("INSERT INTO users (email, password, address) values (:email, :password, :address)");
			$statement->execute($data);
		
			session_start();
		
			$statement = $db->prepare('SELECT * FROM users WHERE email = ? AND password = ?');
			$statement-> bindParam(1, $request->body("username"));
			$statement->bindParam(2, $request->body("password"));
			$statement->execute();
			$resultSet = $statement->fetch();
			echo $resultSet["user_id"];
			// set the user_id in session
			$_SESSION["user_id"] = $resultSet["user_id"];
			// just takes the user back to the home page, automatically logs in 
			$response->redirect("/");
		}
	} 
	
	//echo $username; //-------------------------------- for testing purposes
	
	$response->render('main', 'register', [
	"pageTitle" => "Register",
	"messages" => $messages]);
} ?>