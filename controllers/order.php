<?php return function($request, $response) {
	
	$db = require_once('lib/database.php');
	
	session_start();

	$username = NULL;
	$user_id = $_SESSION["user_id"] ?? NULL;
	$messages = [];
	// for validation
	$regex = "@^.*[^a-zA-Z0-9 \t].*$@";
	
	//if not logged in then redirect to login page passing a status code to indicate that the user was attempting to order
	if ($user_id == NULL) {
		$response->redirect("/login?order=1");
	} else {
			//get the user object
			$statement = $db->query('SELECT * FROM users WHERE user_id = '.$user_id);
			$user = $statement->fetch();
	}
	
	if ($request->method() == "POST") {
		// process insert query
		
		//validations here
		//check if empty	
		if (empty($request->body("item_name"))) {
			$messages["item_name_error"] = "You cannot eat an empty string.";
		}
		//check if there are any special characters
		if (preg_match($regex, $request->body("item_name"), $matches)) {
			$messages["item_name_error"] = "You cannot have illegal characters in the order.";
		}			
			
		//execute query if messages is empty
		if (empty($messages)) {
				$db->query('INSERT INTO orders (user_id, item_name) VALUES ('.$user["user_id"].', "'.$request->body("item_name").'")');
				$response->redirect("/orders?success=1");
		}
	}
	
	//echo $username; //-------------------------------- for testing purposes
	
	$response->render('main', 'order', [
	"pageTitle" => "Order",
	"messages" => $messages,
	"user" => $user
	]);
} ?>